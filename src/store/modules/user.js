// 导入获取本地数据的方法
import { getUserToken, removeUserToken } from '@/utils/storage.js'
// 获取用户信息
import { getUserInfo } from '@/api/user'
export default {
  namespaced: true,
  state: {
    // 存储用户的token
    token: getUserToken(),
    // 存储用户的信息
    userInfo: {}
  },
  getters: {},
  mutations: {
    // 存储用户的token
    setUserToken(state, paload) {
      state.token = paload
    },
    setUserInfo(state, paload) {
      state.userInfo = paload
    },
    removeUserToken(state) {
      state.token = ''
      // 删除本地
      removeUserToken()
    }
  },
  actions: {
    async actions_setUserInfo(context) {
      // 获取用户信息
      const { data: res } = await getUserInfo()
      // console.log(res)
      context.commit('setUserInfo', res.userInfo)
    }
  }
}
