import Vue from 'vue'
import VueRouter from 'vue-router'

// 导入组件
import Layout from '@/views/layout'
import Login from '@/views/login'
import Register from '@/views/register'
import NotFound from '@/views/404'
import List from '@/views/layout/fl/list'
import Article from '@/views/article'
import UserEdit from '@/views/layout/user/edit'
import Myarticle from '@/views/layout/user/myarticle'
import Mysave from '@/views/layout/user/mysave'
import Myzan from '@/views/layout/user/myzan'
import Details from '@/views/layout/home/details'
// 导入二级路由
import Home from '@/views/layout/home'
import FL from '@/views/layout/fl'
import FB from '@/views/layout/fb'
import User from '@/views/layout/user'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Layout,
    redirect: '/home',
    children: [
      {
        path: 'home',
        component: Home
      },
      {
        path: 'fl',
        component: FL
      },
      {
        path: 'list',
        component: List
      },
      {
        path: 'fb',
        component: FB
      },
      {
        path: 'user',
        component: User
      }
    ]
  },
  // 登陆页
  {
    path: '/login',
    component: Login
  },
  // 注册页
  {
    path: '/register',
    component: Register
  },
  // 文章详情
  {
    path: '/article',
    component: Article
  },
  {
    path: '/user/edit',
    component: UserEdit
  },
  {
    path: '/myarticle',
    component: Myarticle
  },
  {
    path: '/mysave',
    component: Mysave
  },
  {
    path: '/myzan',
    component: Myzan
  },
  {
    path: '/details',
    component: Details
  },
  {
    path: '*',
    component: NotFound
  }
]

const router = new VueRouter({
  routes
})

export default router
