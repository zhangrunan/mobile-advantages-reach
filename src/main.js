import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'amfe-flexible'
import '@/utils/vant'
import './styles/base.less'
import moment from 'moment'
// 全局过滤器  参数1 过滤器名字  参数2：过滤器函数
// 中文
moment.locale('zh-cn')
Vue.filter('fromNow', (time) => moment(time).fromNow())
Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
