// 封装本地存储的操作
const TOKEN_NAME = 'hmydd-token'
// 保存token
export const setUserToken = (token) =>
  localStorage.setItem(TOKEN_NAME, JSON.stringify(token))

// 删除token
export const removeUserToken = () => localStorage.removeItem(TOKEN_NAME)

// 获取token
export const getUserToken = () =>
  JSON.parse(localStorage.getItem(TOKEN_NAME)) || {}
