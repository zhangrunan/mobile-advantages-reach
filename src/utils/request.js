/* 封装axios */
import axios from 'axios'
// 需要手动导入store用来获取token
import store from '@/store'
// 推荐的做法 ==> 使用axios来创建axios的实例对象并配置
const instance = axios.create({
  // 根路径
  baseURL: 'http://124.223.14.236:8060/home',
  // 设置超时时间
  timeout: 5000
})

// 添加请求拦截器
instance.interceptors.request.use(
  function (config) {
    // 获取token信息
    const token = store.state.user.token
    // if (token) {
    //   // 有token就带上 没有就算了
    //   // 在发送请求之前做些什么 统一处理token
    config.headers.token = token
    // }
    return config
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 添加响应拦截器
instance.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么  处理数据  剥掉一层data
    return response.data
  },
  function (error) {
    // 对响应错误做点什么
    return Promise.reject(error)
  }
)
export default instance
