// 发送登录的请求
import axios from '@/utils/request'
export const loginFn = (mobile, password) =>
  axios({
    url: '/index/login',
    method: 'POST',
    data: {
      mobile,
      password
    }
  })
