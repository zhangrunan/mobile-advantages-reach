// 获取文章详情
import axios from '@/utils/request'
export const getArticle = (id) =>
  axios({
    method: 'GET',
    url: '/index/show',
    params: {
      id
    }
  })
// 点赞收藏
export const userDataHandle = (type, id, action) =>
  axios({
    method: 'GET',
    url: '/user/userDataHandle',
    params: {
      type,
      article_id: id,
      action
    }
  })
// 发布文章
export const addArticle = (title, author, cateid, tags, pic, content, status) =>
  axios({
    method: 'POST',
    url: '/user/addArticle',
    data: {
      title,
      author,
      cateid,
      tags,
      pic,
      content,
      status
    }
  })
// 上传文件
export const uploadFile = (file) =>
  axios({
    method: 'POST',
    url: '/common/upload?type=images',
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    data: file
  })
// 我的文章
export const myArticle = (limit, page) =>
  axios({
    method: 'GET',
    url: '/user/myArticle',
    params: {
      limit,
      page
    }
  })
// 我的收藏
export const mySave = (limit, page) =>
  axios({
    method: 'GET',
    url: '/user/userDataList',
    params: {
      type: '1',
      limit,
      page
    }
  })

// 我的点赞
export const myZan = (limit, page) =>
  axios({
    method: 'GET',
    url: '/user/userDataList',
    params: {
      type: '2',
      limit,
      page
    }
  })

// 删除我的文章
export const delArticle = (id) =>
  axios({
    method: 'GET',
    url: '/user/delArticle',
    params: {
      id
    }
  })
