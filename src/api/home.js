// 获取首页轮播图的信息
import axios from '@/utils/request'
export const getHome = () =>
  axios({
    method: 'GET',
    url: '/index/index'
  })
// 获取热门推荐
export const getHot = () =>
  axios({
    method: 'GET',
    url: '/index/hot'
  })
// 获取最新更新
export const getNew = (page) =>
  axios({
    method: 'GET',
    url: '/index/new',
    params: {
      page,
      limit: 10
    }
  })

// 热门推荐详情
export const getHotShow = (id) =>
  axios({
    method: 'GET',
    url: '/index/show',
    params: {
      id
    }
  })
