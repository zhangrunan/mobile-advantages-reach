import axios from '@/utils/request'
export const getList = (page, limit, cateid) =>
  axios({
    method: 'GET',
    url: '/index/list',
    params: {
      page,
      limit,
      cateid
    }
  })
