// 发送注册的请求
import axios from '@/utils/request'
export const registerFn = (mobile, password) =>
  axios({
    url: '/index/reg',
    method: 'POST',
    data: {
      mobile,
      password
    }
  })
