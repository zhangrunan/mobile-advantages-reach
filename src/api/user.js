import axios from '@/utils/request'
// import store from '@/store'
export const getUserInfo = () =>
  axios({
    method: 'GET',
    url: '/user/getUserInfo'
  })

export const updateuserInfo = (obj) =>
  axios({
    method: 'POST',
    url: '/user/update',
    data: obj
  })
